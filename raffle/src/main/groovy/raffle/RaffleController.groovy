package raffle

import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.beans.value.ObservableStringValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.ButtonBar
import javafx.scene.control.Dialog
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.web.WebView
import javafx.stage.Modality
import javafx.stage.Stage

import java.text.SimpleDateFormat

class RaffleController {

    RaffleService raffleService

    @FXML
    Label raffleTitle

    @FXML
    Label raffleTime

    @FXML
    ImageView logoView

    @FXML
    List people = FXCollections.observableArrayList ( [ ] )

    @FXML
    public void initialize() {
        Platform.runLater {
            Stage wait = showWait()
            new Thread({
                raffleService.init()

                people.clear()
                people.addAll raffleService.people

                Platform.runLater {
                    raffleTitle.setText( raffleService.meetupTitle )

                    raffleTime.setText( new SimpleDateFormat('yyyy/MM/dd HH:mm').format(new Date()) )

                    logoView.image = new Image("images/${raffleService.meetupGroup}.png")

                    wait.close()
                }
            } as Runnable).start()
        }
    }


    Stage showWait(){
        URL xml = RaffleController.class.getResource("/wait.fxml")
        FXMLLoader loader = new FXMLLoader(xml)
        Parent parent = loader.load()

        Scene scene = new Scene(parent, 500, 200);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.show()

        stage
    }


    void raffle() {
        if( people.size() == 0 )
            return

        Random rnd = new Random()
        int selected = rnd.nextInt(people.size())

        FxRow theWinner = people[selected]

        URL xml = RaffleController.class.getResource("/winner.fxml")
        FXMLLoader loader = new FXMLLoader(xml)
        loader.controller = new WinnerController(winner : theWinner )
        Parent parent = loader.load()

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.title = "The winner is ..."
        alert.headerText = null
        alert.dialogPane.content = parent

        def ret = alert.showAndWait()

        if (ret.get().buttonData == ButtonBar.ButtonData.CANCEL_CLOSE) {
            people.remove selected
            people = people.sort{ Math.random() }
            return
        }

        Stage stage = showWait()
        new Thread({
            raffleService.winner = theWinner
            Platform.runLater{
                stage.close()
                Platform.exit();
                System.exit(0);
            }
        } as Runnable).start()
    }
}

package raffle

import com.google.api.services.sheets.v4.SheetsScopes
import com.google.api.services.gmail.GmailScopes
import com.puravida.groogle.GmailServiceBuilder
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import com.puravida.groogle.SheetService
import com.puravida.groogle.GmailService
import com.puravida.groogle.SheetServiceBuilder
import javafx.fxml.FXML

class RaffleService {

    String credentials="client_secret.json"
    String sheetId
    String tabId
    SheetService sheetService
    GmailService gmailService

    List rangeValues
    List people = []
    String meetupGroup
    String meetupTitle

    void init() {
        String credentialsFile = credentials
        Groogle groogle = GroogleBuilder.build {
            credentials {
                applicationName 'raffle'
                withScopes SheetsScopes.SPREADSHEETS, GmailScopes.MAIL_GOOGLE_COM
                usingCredentials credentialsFile
                asService false
                storeCredentials true
            }
            service(SheetServiceBuilder.build(), SheetService)
            service(GmailServiceBuilder.build(), GmailService)
        }

        sheetService = groogle.service(SheetService) as SheetService
        gmailService = groogle.service(GmailService) as GmailService

        loadRange()
    }

    void loadRange(){
        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                rangeValues = writeRange("A1", "C99").get()
            }
        }
        rangeValues.eachWithIndex{ def entry, int i ->
            if( i == 0) { // config
                meetupGroup = (entry[0] ?: "meetup").toString().toLowerCase()
                meetupTitle = entry[1] ?: "Meetup"
                return
            }
            if( i == 1){ // title rows skyp
                return
            }
            if( entry[0])
                people.add new FxRow(name:entry[0], email: entry[1])
        }
        people.sort { Math.random() }
    }

    void setWinner(FxRow winner){

        rangeValues.eachWithIndex{ def entry, int i ->
            if( i < 2 )
                return
            entry[2] = entry[0] == winner.name ? "Ganador: ${new Date()}".toString() : ""
        }

        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                writeRange("A1", "C99").set(rangeValues)
            }
        }

        if( winner.email && rangeValues[0][2] ){
            gmailService.sendEmail {
                from 'me'
                to winner.email
                subject "Sorteo $meetupTitle ($meetupGroup)"
                body """Enhorabuena ${winner.name}
                ${rangeValues[0][2]}
                """
            }
        }
    }

}

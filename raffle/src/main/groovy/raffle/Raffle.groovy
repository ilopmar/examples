package raffle

import groovy.transform.CompileStatic
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

@CompileStatic
class Raffle extends Application {

    @Override
    void start(Stage primaryStage) throws Exception {

        RaffleService raffleService = new RaffleService(
                sheetId: parameters.raw[0], tabId: parameters.raw[1])

        if( parameters.getRaw().size() > 2)
            raffleService.credentials = parameters.raw[2]

        RaffleController controller = new RaffleController(raffleService: raffleService)

        URL xml = Raffle.class.getResource("/raffle.fxml")
        FXMLLoader loader = new FXMLLoader(xml)
        loader.controller = controller

        Parent root = loader.load()
        primaryStage.title = "Groogle Raffle"
        Scene scene = new Scene(root, 1024, 768)
        primaryStage.setScene(scene)
        primaryStage.show();

    }

    static void main(String ...args){
        if ( args.length< 2 ) {
            println "Necesito el Id de la hoja y el Tab con los asistentes"; return
        }
        launch(args)
    }
}
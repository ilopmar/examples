package raffle

import groovy.transform.Canonical

@Canonical
class FxRow {
    String name
    String email
}
